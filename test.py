from ket import *
from quantuloop import client

client.set_server('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cmwiOiJodHRwOi8vMTI3LjAuMC4xOjQyNDIifQ.Ps6-MiRiXBTgdP-1xZZsE00sPcTGlv-zH4kHPErHGb8')


print(client.available_simulators())

client.set_simulator("Quantuloop Sparse")

q = quant(23)
H(q)
Z(q)
H(q)
print(dump(q).show())
