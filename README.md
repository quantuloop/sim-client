![Quantuloop Quantum Simulator Suite for HPC](https://quantuloop.com.br/_images/qlhpc-light.svg)

# Quantum Simulator Suite Client

## Install

```shell
pip install git+https://gitlab.com/quantuloop/client.git
```

## Usage

* Setup server:

    ```py
    from quantuloop import client
    client.set_server('SERVER_TOKEN')
    ```

* Change quantum simulator:

    ```py
    client.set_simulator('Simulator Name')
    ```

* Get a list of the available quantum simulators:

    ```py
    client.available_simulators()
    ```
