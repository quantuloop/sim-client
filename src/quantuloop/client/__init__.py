from __future__ import annotations
import requests
from ket.clib.libket import JSON
from ket.base import set_quantum_execution_target
from ctypes import *
import jwt

_token = None
_url = None
_available_simulators = None
_simulator = ''
_precision = 1
_ngpu = 1


def available_simulators() -> list[str]:
    """Get and update the list of the available simulators"""

    global _available_simulators
    _available_simulators = requests.get(_url+'/simulators').json()
    return _available_simulators


def set_simulator(simulator: str, precision: int = 1, number_GPUs: int = 1):
    """Set the quantum simulator

    You must run first :func:`quantuloop.client.set_server`.
    Otherwise, the quantum execution will fail.

    Some quantum simulators are not affected by the parameters "precision" and "number_GPUs".

    Args:
        simulator: Set simulation. See func:`quantuloop.client.available_simulators`.
        precision: Set the floating point precision. Acceptable values are 
                   "1" for single precision and "2" for double precision.
        number_GPUs: Set the number of GPUs used in the simulation.
    """

    simulator = simulator.upper()
    if simulator not in _available_simulators:
        raise ValueError(
            f"parameter 'mode' must be in {_available_simulators}")
    if int(precision) not in [1, 2]:
        raise ValueError("parameter 'precision' must be '0' or '1'")

    global _simulator
    _simulator = simulator
    global _precision
    _precision = precision
    global _ngpu
    _ngpu = number_GPUs


def _send_quantum_code(process):
    process.serialize_quantum_code(JSON)
    process.serialize_metrics(JSON)

    code_data, code_size, _ = process.get_serialized_quantum_code()
    metrics_data, metrics_size, _ = process.get_serialized_metrics()

    result = requests.get(_url+'/run',
                          params=dict(
                              simulator=_simulator,
                              precision=_precision,
                              ngpu=_ngpu,
                          ),
                          files=dict(
                              token=('token.jwt', _token.encode(
                                  'utf-8'), 'text/plain'),
                              quantum_code=('quantum_code.json', bytearray(
                                  code_data[:code_size.value]), 'text/plain'),
                              quantum_metrics=('quantum_metrics.json', bytearray(
                                  metrics_data[:metrics_size.value]), 'text/plain')
                          )).content
    result_size = len(result)

    process.set_serialized_result(
        (c_uint8*result_size)(*result),
        result_size,
        JSON
    )


def set_server(token: str):
    """Set the Server Token for quantum execution"""
    global _token
    _token = token
    global _url
    _url = jwt.decode(token, options={"verify_signature": False})['url']
    available_simulators()
    set_quantum_execution_target(_send_quantum_code)
